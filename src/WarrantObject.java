
public class WarrantObject {
	public static enum OFFENSE_CLASS{FM,FS,FX,F1,F2,F3,F4,MA,MB,MC,P}
	public static enum CASE_TYPE{CF,CM,DT,TR,OV,JD,CV,CC}
	private Enum<OFFENSE_CLASS> offenseClass;
	private Enum<CASE_TYPE> caseType;
	private String documentFileName;
	private Enum<COUNTY> originatingCounty;
	private String casePhase;
	
	public WarrantObject(){
		this.offenseClass=OFFENSE_CLASS.MA;
		this.caseType=CASE_TYPE.CM;
		this.documentFileName="1101.pdf";
		this.originatingCounty=COUNTY.DeKalb;
		this.casePhase="PR";
				
	}
	
	public Enum<CASE_TYPE> getCaseType() {
		return caseType;
	}

	public void setCaseType(Enum<CASE_TYPE> caseType) {
		this.caseType = caseType;
	}

	public Enum<OFFENSE_CLASS> getOffenseClass() {
		return offenseClass;
	}
	public void setOffenseClass(Enum<OFFENSE_CLASS> offenseClass) {
		this.offenseClass = offenseClass;
	}
	public String getDocumentFileName() {
		return documentFileName;
	}
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	public Enum<COUNTY> getOriginatingCounty() {
		return originatingCounty;
	}

	public void setOriginatingCounty(Enum<COUNTY> originatingCounty) {
		this.originatingCounty = originatingCounty;
	}

	public String getCasePhase() {
		return casePhase;
	}
	public void setCasePhase(String casePhase) {
		this.casePhase = casePhase;
	}
}
