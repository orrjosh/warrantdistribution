(deftemplate countyBorderRelationship (slot county) (slot neighbor))
(deftemplate Warrant
    (declare (from-class WarrantObject)))
(deftemplate Service
    (declare (from-class ServiceObject)))
(deftemplate serviceType (slot geoLimit))


(defrule misdemeanorType "Serve Misdemeanor warrants in surrounding counties" 
    (declare (salience 20))
    ?W <-(Warrant (caseType CM))
    =>
    (assert (serviceType (geoLimit surrounding_county)))
    )

(reset)
(deffacts counties
    (countyBorderRelationship (county Kane) (neighbor Dekalb))
    (countyBorderRelationship (county Dekalb) (neighbor Kendall))
    (countyBorderRelationship (county Kane) (neighbor Kendall))
    (countyBorderRelationship (county Dekalb) (neighbor Ogle))
    (Warrant)
    )
(reset)
(run)
(facts)