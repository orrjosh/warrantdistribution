
public class ServiceObject {
	String county;
	WarrantObject warrant;
	
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public WarrantObject getWarrant() {
		return warrant;
	}
	public void setWarrant(WarrantObject warrant) {
		this.warrant = warrant;
	}
	
	
}
